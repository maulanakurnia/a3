package main

import (
	"github.com/julienschmidt/httprouter"
	"golang-mongodb/controllers"
	"gopkg.in/mgo.v2"
	"net/http"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())

	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)

	http.ListenAndServe("127.0.0.1:9000", r)
}

func getSession() *mgo.Session {
	sess, err := mgo.Dial("mongodb://localhost:27017")

	if err != nil {
		panic(err)
	}

	return sess
}

