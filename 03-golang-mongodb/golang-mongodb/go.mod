module golang-mongodb

go 1.19

require (
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	go.mongodb.org/mongo-driver v1.11.1 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
