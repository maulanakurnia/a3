# Assigment 3 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin
<br >
Position : Intern Backend

This repository contains A3 assignment that include:
1. [Golang - Mux API](https://gitlab.com/maulanakurnia/a3/-/tree/main/01-golang-mux-api)
2. [Golang - Web API](https://gitlab.com/maulanakurnia/a3/-/tree/main/02-golang-web-api)
3. [Golang - MongoDB](https://gitlab.com/maulanakurnia/a3/-/tree/main/03-golang-mongodb)
4. [Golang - MongoDB Atlas](https://gitlab.com/maulanakurnia/a3/-/tree/main/04-golang-mongodb-atlas)
5. [Postman Automation](https://gitlab.com/maulanakurnia/a3/-/tree/main/05-postman-automation)