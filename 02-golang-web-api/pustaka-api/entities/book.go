package entities

import "time"

type Book struct {
	ID          uint			`json:"id" gorm:"primaryKey"`
	Title       string			`json:"title"`
	Description string			`json:"description"`
	Price       int				`json:"price"`
	Discount    int				`json:"discount"`
	Rating      int				`json:"rating"`
	CreatedAt   time.Time		`json:"created_at"`
	UpdatedAt   time.Time		`json:"updated_at"`
}

func (Book) TableName() string {
	return "books"
}
