# Golang API
in this project, i will try to make a simple web API written in Go. It uses the [Gin](https://github.com/gin-gonic/gin) framework.
base from Youtube Channel [Agung Setiawan](https://youtu.be/GjI0GSvmcSU) tutorial.

## Getting Started
1. You first need Go installed (version 1.16+ is required), then you can use the below Go command to install Gin.
```bash
$ go get -u github.com/gin-gonic/gin
```
2. Install Gorm 
```bash
$ go get -u gorm.io/gorm
$ go get -u gorm.io/driver/mysql
```

Happy Coding!

## API Routes
| Endpoint | HTTP Route    | Description       |
|----------|---------------|-------------------|
| GET      | /v1/books     | Get all books     |
| GET      | /v1/books/:id | Get book by id    |
| POST     | /v1/books     | Create new book   |
| PUT      | /v1/books/:id | Update book by id |
| DELETE   | /v1/books/:id | Delete book by id |

## Test Endpoints
### Get all books

```bash
# Request
curl --location --request GET 'http://127.0.0.1:8080/v1/books'
```

```bash
# Response
{
    "data": [
        {
            "id": 2,
            "title": "Atomic Habbits",
            "price": 120000,
            "discount": 10,
            "rating": 2,
            "description": "Self Improvement Book"
        },
        {
            "id": 3,
            "title": "Kancil dan Kura-Kura",
            "price": 25000,
            "discount": 100,
            "rating": 3,
            "description": "buku kancil dan kura-kura"
        },
      ... more data
    ]
}
```

### Get book by id

```bash
# Request
curl --location --request GET 'http://127.0.0.1:8080/v1/books/2'
```

```bash
# Response
{
    "data": {
        "id": 2,
        "title": "Atomic Habbits",
        "price": 120000,
        "discount": 10,
        "rating": 2,
        "description": "Self Improvement Book"
    }
}
```

### Create new book

```bash
# Request
curl --location --request POST 'http://127.0.0.1:8080/v1/books' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Cara Menjadi Jagoan",
    "price": 60000,
    "discount": 5,
    "rating": 2,
    "description": "Membantu anda menjadi jagoan"
}'
```

```bash
# Response
{
    "data": {
        "id": 8,
        "title": "Cara Menjadi Jagoan",
        "price": 60000,
        "discount": 5,
        "rating": 2,
        "description": "Membantu anda menjadi jagoan"
    }
}
```

### Update book by id

```bash
# Request
curl --location --request PUT 'http://127.0.0.1:8080/v1/books/2' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Atomic Habbits 2nd Edition",
    "price": 125000,
    "discount": 5,
    "rating": 2,
    "description": "Self Improvement Book"
}'
```

```bash
# Response
{
    "data": {
        "id": 2,
        "title": "Atomic Habbits 2nd Edition",
        "price": 125000,
        "discount": 5,
        "rating": 2,
        "description": "Self Improvement Book"
    }
}
```

### Delete book by id

```bash
# Request
curl --location --request DELETE 'http://127.0.0.1:8080/v1/books/2'
```

```bash
# Response
{
    "message": "Data has been deleted"
}
```

## Explanation
### Package Gin
Gin is a web framework written in Go (Golang). It features a martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
### Entity Layer
In this layer, we will create the entity layer. The entity layer is the layer that contains the data structure that will be used in the application. In this project, we will create a User entity. The User entity will be used to store the user data that will be used in the application.
### Repository Layer
Repository layer is a layer that is responsible for accessing data from the database. This layer is also responsible for converting the data from the database into a format that can be used by the service layer.
### Service Layer
Service layer is a layer that is responsible for processing the data from the repository layer. This layer is also responsible for processing the data that will be sent to the repository layer.
### Handler Layer
Handler layer is a layer that is responsible for handling the request from the client. This layer is also responsible for sending the response to the client.