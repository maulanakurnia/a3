package main

import (
	"github.com/gin-gonic/gin"
	"golang-api/config"
	"golang-api/handler"
	"golang-api/repository"
	"golang-api/service"
	"log"
)

func main() {
	db := config.InitDB()

	router := gin.Default()
	err := router.SetTrustedProxies([]string{"127.0.0.1"})
	if err != nil {
		return
	}

	bookRepository := repository.NewBookRepository(db)
	bookService := service.NewBookService(bookRepository)
	handler.CreateBookHandler(router, bookService)

	err = router.Run(":8000")
	if err != nil {
		log.Fatal(err)
	}
}

