package service

import (
	"golang-api/entities"
	"golang-api/repository"
	"golang-api/utils"
)

type IBookService interface {
	Create(book utils.BookRequest) (entities.Book, error)
	GetBooks() ([]entities.Book, error)
	GetBookById(id int) (entities.Book, error)
	UpdateBook(id int, book utils.BookRequest) (entities.Book, error)
	DeleteBook(id int) error
}

type BookService struct {
	BookRepository repository.IBookRepository
}

func NewBookService(bookRepository repository.IBookRepository) *BookService {
	return &BookService{BookRepository: bookRepository}
}

func (b BookService) Create(bookRequest utils.BookRequest) (entities.Book, error) {
	price, _ := bookRequest.Price.Int64()
	discount, _ := bookRequest.Discount.Int64()
	rating, _ := bookRequest.Rating.Int64()
	book := entities.Book{
		Title:       bookRequest.Title,
		Price:       int(price),
		Discount:    int(discount),
		Rating:      int(rating),
		Description: bookRequest.Description,
	}

	return b.BookRepository.Create(book)
}

func (b BookService) GetBooks() ([]entities.Book, error) {
	return b.BookRepository.GetBooks()
}

func (b BookService) GetBookById(id int) (entities.Book, error) {
	return b.BookRepository.GetBook(id)
}

func (b BookService) UpdateBook(id int, book utils.BookRequest) (entities.Book, error) {
	return b.UpdateBook(id, book)
}

func (b BookService) DeleteBook(id int) error {
	return b.BookRepository.DeleteBook(id)
}


