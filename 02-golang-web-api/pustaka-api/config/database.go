package config

import (
	"golang-api/entities"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)


func InitDB() *gorm.DB {
	dsn := "root:root@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf("failed to connect database %v : %v", dsn, err.Error())
		return nil
	}

	err = db.Debug().AutoMigrate(entities.Book{})
	if err != nil {
		log.Fatalf("failed to migrate database %v : %v", dsn, err.Error())
		return nil
	}
	return db
}
