package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"golang-api/entities"
	"golang-api/service"
	"golang-api/utils"
	"net/http"
	"strconv"
)

type BookHandler struct {
	BookService service.IBookService
}

func CreateBookHandler(r *gin.Engine, bookUseCase service.IBookService) {
	handler := &BookHandler{BookService: bookUseCase}

	v1 := r.Group("/v1")
	v1.GET("/books", handler.GetBooks)
	v1.GET("/books/:id", handler.GetBookById)
	v1.POST("/books", handler.CreateBook)
	v1.PUT("/books/:id", handler.UpdateBook)
	v1.DELETE("/books/:id", handler.DeleteBook)
}

// convertToBookResponse
// @Description Convert book to book response
// @Param book entities.Book true "Book"
////**
func convertToBookResponse(book entities.Book) utils.BookResponse {
	return utils.BookResponse{
		ID:    book.ID,
		Title: book.Title,
		Price: book.Price,
		Description: book.Description,
		Rating: book.Rating,
		Discount: book.Discount,
	}
}

// CreateBook
// @Description Create new book
////**
func (b BookHandler) CreateBook(ctx *gin.Context) {
	var bookRequest utils.BookRequest

	err := ctx.ShouldBindJSON(&bookRequest)

	if err != nil {
		var error_request []string

		if vals, ok := err.(validator.ValidationErrors); ok {
			for _, e := range vals {
				errorMessage := fmt.Sprintf("Error field %s, Condition %s", e.Field(), e.ActualTag())
				error_request = append(error_request, errorMessage)
				ctx.JSON(http.StatusBadRequest, errorMessage)
				return
			}
		}
	}

	book, err := b.BookService.Create(bookRequest)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

// GetBooks
// @Description Get all books
///**
func (b BookHandler) GetBooks(ctx *gin.Context) {
	books, err := b.BookService.GetBooks()

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	var booksResponse []utils.BookResponse

	for _, book := range books {
		booksResponse = append(booksResponse, convertToBookResponse(book))
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})
}

// GetBook
// @Description Get book by id
// @Param id path int true "Book ID"
////**
func (b BookHandler) GetBookById(ctx *gin.Context) {
	bookId := ctx.Param("id")
	id, _ := strconv.Atoi(bookId)

	book, err := b.BookService.GetBookById(id)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

// UpdateBook
// @Description Update book by id
// @Param id path int true "Book ID"
////**
func (b BookHandler) UpdateBook(ctx *gin.Context) {
	bookId := ctx.Param("id")
	id, _ := strconv.Atoi(bookId)

	var bookRequest utils.BookRequest

	err := ctx.ShouldBindJSON(&bookRequest)

	if err != nil {
		var error_request []string

		if vals, ok := err.(validator.ValidationErrors); ok {
			for _, e := range vals {
				errorMessage := fmt.Sprintf("Error field %s, Condition %s", e.Field(), e.ActualTag())
				error_request = append(error_request, errorMessage)
				ctx.JSON(http.StatusBadRequest, errorMessage)
				return
			}
		}
	}

	book, err := b.BookService.UpdateBook(id, bookRequest)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

// DeleteBook
// @Description Delete book by id
// @Param id path int true "Book ID"
////**
func (b BookHandler) DeleteBook(ctx *gin.Context) {
	bookId := ctx.Param("id")
	id, _ := strconv.Atoi(bookId)

	err := b.BookService.DeleteBook(id)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "Data has been deleted",
	})
}