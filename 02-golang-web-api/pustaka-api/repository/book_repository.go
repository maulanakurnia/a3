package repository

import (
	"fmt"
	"golang-api/entities"
	"gorm.io/gorm"
)

type IBookRepository interface {
	Create(book entities.Book) (entities.Book, error)
	GetBooks() ([]entities.Book, error)
	GetBook(id int) (entities.Book, error)
	UpdateBook(id int, book entities.Book) (entities.Book, error)
	DeleteBook(id int) error
}

func NewBookRepository(db *gorm.DB) IBookRepository {
	return &BookRepository{DB: db}
}

type BookRepository struct {
	DB *gorm.DB
}

func (b BookRepository) Create(book entities.Book) (entities.Book, error) {
	var newBook = entities.Book{}
	err := b.DB.Table("books").Create(book).Error

	if err != nil {
		fmt.Println("[BookRepository.Create]")
		fmt.Printf("[ERROR] Gagal melakukan eksekusi query: %v \n", err)
	}

	return newBook, err
}

func (b BookRepository) GetBooks() ([]entities.Book, error) {
	var books []entities.Book

	err := b.DB.Table("books").Find(&books).Error

	if err != nil {
		fmt.Println("[BookRepository.GetBooks]")
		fmt.Printf("[ERROR] Gagal melakukan eksekusi query: %v \n", err)
	}

	return books, nil
}

func (b BookRepository) GetBook(id int) (entities.Book, error) {
	var book = entities.Book{}

	err := b.DB.Table("books").Find(&book, id).Error

	if err != nil {
		fmt.Println("[BookRepository.GetBook]")
		fmt.Printf("[ERROR] Gagal melakukan eksekusi query: %v \n", err)
	}

	return book, nil
}

func (b BookRepository) UpdateBook(id int, book entities.Book) (entities.Book, error) {
	var updateBook = entities.Book{}

	err := b.DB.Table("books").Where("id = ?", id).First(&updateBook).Updates(&book).Error

	if err != nil {
		fmt.Println("[BookRepository.UpdateBook]")
		fmt.Printf("[ERROR] Gagal melakukan eksekusi query: %v \n", err)
	}

	return updateBook, err
}

func (b BookRepository) DeleteBook(id int) error {
	var deleteBook = entities.Book{}

	err := b.DB.Table("books").Where("id = ?", id).First(&deleteBook).Delete(&deleteBook).Error

	if err != nil {
		fmt.Println("[BookRepository.DeleteBook]")
		fmt.Printf("[ERROR] Gagal melakukan eksekusi query: %v \n", err)
		return fmt.Errorf("gagal menemukan data")
	}

	return nil
}
