package utils

type BookResponse struct {
	ID          uint   `json:"id"`
	Title       string `json:"title"`
	Price       int    `json:"price"`
	Discount    int    `json:"discount"`
	Rating      int    `json:"rating"`
	Description string `json:"description"`
}
