package main

import (
	"encoding/json" // for json encoding
	"fmt"
	"github.com/gorilla/mux" // for routing
	"log"                    // for logging
	"math/rand"              // for generating random numbers
	"net/http"               // for http requests
	"strconv"                // for converting strings to integers
)

// Book Struct (Model)
type Book struct {
	ID     string  `json:"id"`
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author *Author `json:"author"`
}

// Author struct
type Author struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

// Init books var as a slice Book struct
var books []Book

/**
 * mux is a powerful URL router and dispatcher for golang
 * it matches the URL of each incoming request against a list of registered routes
 * this code use package from: https://github.com/gorilla/mux
 * to install it, run: go get -u github.com/gorilla/mux
 * but, author has been archived this package,
 * it means package is no longer maintained.
 * but, it is still working.
 */
func main() {
	// initial router
	router := mux.NewRouter()

	// Mock data
	books = append(books, Book{
		ID:    "1",
		Isbn:  "448743",
		Title: "Book One",
		Author: &Author{
			Firstname: "John",
			Lastname:  "Doe",
		},
	})
	books = append(books, Book{
		ID:    "2",
		Isbn:  "448744",
		Title: "Book Two",
		Author: &Author{
			Firstname: "Steve",
			Lastname:  "Smith",
		},
	})

	fmt.Println("[INFO] - Server is running on port 8000")
	fmt.Println("[INFO] - Try to access: http://localhost:8000/api/books")
	fmt.Println("[INFO] - if you want to test API, you can use Postman.")

	// router handler / endpoint
	router.HandleFunc("/api/books", getBooks).Methods("GET")
	router.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	router.HandleFunc("/api/books", createBook).Methods("POST")
	router.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	router.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8000", router))
}

// Get all books
func getBooks(res http.ResponseWriter, req *http.Request) {
	// set content type to json
	res.Header().Set("Content-Type", "application/json")
	// encode the books slice as json and send it as the response
	json.NewEncoder(res).Encode(books)
}

// Get single book
func getBook(res http.ResponseWriter, req *http.Request) {
	// set content type to json
	res.Header().Set("Content-Type", "application/json")
	// get params
	params := mux.Vars(req)
	// Loop through books and find one with the id from the params
	for _, item := range books {
		// if the id is found,
		// encode the item as json and send it as the response
		if item.ID == params["id"] {
			// encode the item as json and send it as the response
			json.NewEncoder(res).Encode(item)
			return
		}
	}

	// if the id is not found,
	json.NewEncoder(res).Encode(&Book{})
}

// Add new book
func createBook(res http.ResponseWriter, req *http.Request) {
	// set content type to json
	res.Header().Set("Content-Type", "application/json")
	// create a new book
	var book Book
	// decode the request body into the new book
	_ = json.NewDecoder(req.Body).Decode(&book)

	/**
	 * generate a random id for the new book
	 * strconv.Itoa() converts an integer to a string
	 * this is not a secure way to generate ids
	 * because it is possible to generate the same id twice
	 */
	book.ID = strconv.Itoa(rand.Intn(100000000))

	// add the new book to the books slice
	books = append(books, book)

	// encode the new book as json and send it as the response
	json.NewEncoder(res).Encode(book)
}

// Update book
func updateBook(res http.ResponseWriter, req *http.Request) {
	// set content type to json
	res.Header().Set("Content-Type", "application/json")
	// get params
	params := mux.Vars(req)
	// Loop through books and find one with the id from the params
	for index, item := range books {
		// if the id is found,
		if item.ID == params["id"] {
			// create a new book
			books = append(books[:index], books[index+1:]...)
			// decode the request body into the new book
			var book Book
			_ = json.NewDecoder(req.Body).Decode(&book)

			// generate a random id for the new book
			book.ID = params["id"]
			// add the new book to the books slice
			books = append(books, book)
			// encode the new book as json and send it as the response
			json.NewEncoder(res).Encode(book)
			return
		}
	}
}

// Delete book
func deleteBook(res http.ResponseWriter, req *http.Request) {
	// set content type to json
	res.Header().Set("Content-Type", "application/json")
	// get params from the request
	params := mux.Vars(req)
	// Loop through books and find one with the id from the params
	for index, item := range books {
		// if the id is found,
		if item.ID == params["id"] {
			// delete the book
			books = append(books[:index], books[index+1:]...)
			break
		}
	}
	// encode the new book as json and send it as the response
	json.NewEncoder(res).Encode(books)
}
