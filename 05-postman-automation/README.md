# POSTMAN BEGINER'S COURSE - API TESTING
Link Youtube: https://youtu.be/VywxIQ2ZXw4 <br>
API URL	: https://simple-books-api.glitch.me <br>
API Doc	: https://github.com/vdespa/introduction-to-postman-course/blob/main/simple-books-api.md 

## INTRODUCTION
**Assigment 1** : Read Documentation and use limit query params and add in request then try out the diferrent values <br>
**Assigment 2** : Create post request with stock bookId 0 and book not in stock (not available) then see what's happening<br>
**Assigment 3** : cari tau apakah endpoint orders dapat melihat 1 order id atau tidak

### CATATAN
ada 2 cara untuk mendapatkan data buku pada API postman:
- menggunakan slash value -> ex: (/value)
- menggunakan slash path variable -> ex : (/:namePathVariable) namun mengunnakan path variable agak sedikit ribet dibandingkan dengan mengetikkan value langsung, kita harus menset value `path variable` pada section params pada postman.

## API Auth
untuk mengenerate random nama lengkap postman memiliki fungsi yang dinamakan $randomFullName lalu untuk melihat nama hasil generate dari $randomFullName buka console pada pojok kiri bawah urutan ke 3


Fokus pada aplikasi postman :
- berkomunikasi dengan aplikasi ofc
- security testing (warning, tapi ini bisa saja terjadi namun postman bukan aplikasi yang tepat untuk ini)

`204 content` itu sama dengan `200 content`

### Test automation with Postman 
**Assigment 4** : write a status code for all the reqs inside the collection and make sure that those tests will fail <br>
**Assigment 5** : create expectation on my own and expect that this book is from the type non-fiction <br>
**Assigment 6** : set orderId to Request Get Single Book and Order Book 

### CATATAN
`Collection Runner` berfungsi untuk menjalankan semua request tanpa perlu menjalankan satu-satu. tombol collection runner ada pada kanan bawah yang bermana Runner
