package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"os"
	"time"
)

type Podcast struct {
	//ID     primitive.ObjectID `bson:"_id,onitempty"`
	Title  string   `bson:"title,onitempty"`
	Author string   `bson:"author,onitempty"`
	Tags   []string `bson:"tags,onitempty"`
}

func main() {

	/*
		GET ENVIRONMENT VARIABLES
	*/
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	host := os.Getenv("ATLAS_HOST")
	username := os.Getenv("ATLAS_USERNAME")
	password := os.Getenv("ATLAS_PASSWORD")
	dbName := os.Getenv("ATLAS_DBNAME")
	clusterName := os.Getenv("ATLAS_CLUSTER")

	/*
		CONNECT TO MONGODB ATLAS
	*/
	uri := fmt.Sprintf("mongodb+srv://%s:%s@%s/?retryWrites=true&w=majority", username, password, host)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	/*
		LIST ALL DATABASES
	*/
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Databases: %v\n", databases)

	/*
		Get collection
	*/
	database := client.Database(dbName)
	podcastsCollection := database.Collection(clusterName)

	/*
		INSERT ONE DOCUMENT
	*/
	podcast := Podcast{
		Title:  "Developer Nodejs",
		Author: "Maulana Kurnia",
		Tags:   []string{"development", "gaming", "coding"},
	}
	insertResult, err := podcastsCollection.InsertOne(ctx, podcast)
	if err != nil {
		panic(err)
	}
	fmt.Println(insertResult.InsertedID)

	/*
		INSERT MANY DOCUMENTS
	*/

	podcasts := []interface{}{
		Podcast{
			Title:  "Developer Javascript",
			Author: "Fiqih Ainul Yaqin",
			Tags:   []string{"development", "gaming", "coding"},
		},
		Podcast{
			Title:  "Steamer YouTube",
			Author: "Jhon Doe",
			Tags:   []string{"streamer", "gaming", "coding"},
		},
	}

	insertManyResult, err := podcastsCollection.InsertMany(ctx, podcasts)
	if err != nil {
		panic(err)
	}
	fmt.Println(insertManyResult.InsertedIDs)

}
