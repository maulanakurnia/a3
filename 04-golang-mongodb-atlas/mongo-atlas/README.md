# Golang Mongo DB Atlas
This is a simple example of how to connect to a Mongo DB Atlas cluster using Golang.

## Install Package
```bash
$ go get go.mongodb.org/mongo-driver/
```